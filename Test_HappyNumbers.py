from HappyNumbers import categorize_numbers


def test_categorize_numbers():
    assert list(categorize_numbers([1, 19, 4])) == [True, True, False]
