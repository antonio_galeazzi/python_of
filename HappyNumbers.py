import functools
import sys
import time


def head(lst):
    return lst[0] if len(lst) > 0 else []


def tail(lst):
    return lst[1:] if len(lst) > 1 else []


def own_map(f, lst):
    """select in C#"""
    if len(lst) == 0:
        return []
    else:
        return [f(head(lst))] + own_map(f, tail(lst))
    # for i, item in enumerate(lst):
        # lst[i] = f(item)
    # return lst
    # res = []
    # for item in lst:
        # res.append(f(item))
    # return res


def tail_recursive_map(f, lst):
    def accumulate_map(result, f_inner, lst_inner):
        if len(lst_inner) == 0:
            return result
        else:
            result.append(f_inner(head(lst_inner)))
            return accumulate_map(result, f_inner, tail(lst_inner))

    return accumulate_map([], f, lst)


def own_filter(f, lst):
    """where in C#"""
    if len(lst) == 0:
        return []
    else:
        return ([head(lst)] if f(head(lst)) else []) + own_filter(f, tail(lst))


def hello_world():
    print("Hello World!")


def cast_to_int(raw_input):
    try:
        return int(raw_input)
    except ValueError:
        return None


def filter_input(checked_input):
    return checked_input is not None


def process_args(args, filter_func, cast_func):
    return [arg for arg in map(cast_func, args) if filter_func(arg)]
    # return list(filter(filter_func, map(cast_func, args)))
    # res = []
    # for arg in args:
    # casted_arg = cast_func(arg)
    # if filter_func(casted_arg):
    # res.append(casted_arg)
    # return res


def square_numbers(numbers):
    def square_number(number):
        return number ** 2

    return [number**2 for number in numbers]
    # return map(square_number, numbers)
    # squared_numbers = []
    # for number in numbers:
    # squared_numbers.append(number**2)
    # return squared_numbers


def fragment_number(number):
    if 0 <= number <= 9:
        return [number]
    else:
        return fragment_number(number // 10) + [number % 10]


def calculate_sum(number_fragments):
    """aggregate in C#"""
    return functools.reduce(lambda x, y: x + y, number_fragments)
    # fragments_sum = 0
    # for fragment in number_fragments:
    # fragments_sum += fragment
    # return fragments_sum


def categorize_number(number):
    # if number in [4, 16, 37, 58, 89, 145, 42, 20]:
    if number == 4:
        return False
    if number == 1:
        return True
    else:
        # return categorize_number(calculate_sum(square_numbers(fragment_number(number))))
        return categorize_number(compose_funcs([fragment_number,
                                                square_numbers,
                                                calculate_sum],
                                               number))


def categorize_numbers(numbers):
    return map(categorize_number, numbers)


def compose_funcs(funcs, value):
    if len(funcs) == 0:
        return value
    else:
        return compose_funcs(tail(funcs), head(funcs)(value))


def main():
    filtered_args = process_args(sys.argv[1:], filter_input, cast_to_int)
    result = categorize_numbers(filtered_args)
    print("Result: (Input, HappyNumber?)\n" + str(list(zip(filtered_args, result))))


if __name__ == '__main__':
    # print(own_filter(lambda x: x > 0, [-1, -2, 3, -5, 1, 2]))
    # print(tail_recursive_map(lambda x: x ** x, [1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 1000, 10000, 100000]))
    # print(own_map(lambda x: x ** x, [1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 1000, 10000, 100000]))
    main()
